package org.launchcode.launchcart.controllers.rest;

import org.launchcode.launchcart.data.ItemRepository;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/items")
public class ItemRestController {

    @Autowired
    ItemRepository itemRepository;

    @GetMapping("")
    public List<Item> getAllItems(@RequestParam Optional<Double> price){
        if (price.isPresent()) {
            return itemRepository.findByPrice(price.get());
        }
        return itemRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity getItem(@PathVariable int id) {
        Item item = itemRepository.findOne(id);
        if (item == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(item, HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity createItem(@RequestBody Item item) {
        return new ResponseEntity(itemRepository.save(item), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity updateItem(@PathVariable int id, @RequestBody Item item) {
        if (itemRepository.findOne(id) == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        if (id != item.getUid()){
            return new ResponseEntity((HttpStatus.CONFLICT));
        }
        itemRepository.save(item);
        return new ResponseEntity(item, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteItem(@PathVariable int id) {
        Item item = itemRepository.findOne(id);
        if (item == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        itemRepository.delete(item);
        return new ResponseEntity(HttpStatus.OK);
    }

}
